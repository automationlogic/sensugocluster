# SensuGo Cluster provisioning with Terraform and Ansible
Terraform templates for provisioning a sensu-go cluster with terraform and ansible

## Getting started

```
cd basic
cp terraform.tfvars.example terraform.tfvars
```

After doing the template copy above, you can now populate the variables as you wish.

```
terraform init
terraform apply
```
